<!-- PROJECT: AUTO-GENERATED DOCS START (do not remove) -->

# 🪐 项目: Oh My GPT!

### 🗣️ 说明

本项目旨在复现中文版本InstructGPT模型.


### 🗂 安装

- 本项目基于pytorch2.0开发, 需要手动安装.

- 其余依赖可通过requirements.txt文件安装

```
pip install -r requirements.txt
```

- 本项目通过poetry打包发布,可通过下面命令安装poetry
```bash
pip install pipx 
pipx install poetry
pipx ensurepath
```

### 📋 文件

- [`project.yml`](project.yml)文件用于定义该项目所有的数据,命令和流程.


### ⏯ 命令

- 下面是[project.yml](project.yml)文件中定义的命令, 它们都可以通过运行`project run [name]`执行.

| Command | Description |
| --- | --- |
| `init` | 初始化项目 |
| `package` | 打包项目 |
| `install` | 安装当前版本 |
| `preprocess` | 数据预处理 |

### ⏭ 流程

- 流程是命令的组合,它们同样可以通过执行`project run [name]`命令来运行
- 流程会按照定义中的顺序依次执行

| Workflow | Steps |
| --- | --- |
| `train-base` | `preprocess` |
| `build` | `package` &rarr; `install` |


<!-- PROJECT: AUTO-GENERATED DOCS END (do not remove) -->