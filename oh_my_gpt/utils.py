import catalogue
import confection
from confection import Config, ConfigValidationError
from rich.console import Console


console = Console()


class registry(confection.registry):
    """注册表"""
    
    models = catalogue.create("oh_my_gpt", "models", entry_points=True)
    tokenizers = catalogue.create("oh_my_gpt", "tokenizers", entry_points=True)
    datamodules = catalogue.create("oh_my_gpt", "datamodules", entry_points=True)
    callbacks = catalogue.create("oh_my_gpt", "callbacks", entry_points=True)
    loggers = catalogue.create("oh_my_gpt", "loggers", entry_points=True)
    trainers = catalogue.create("oh_my_gpt", "trainers", entry_points=True)

    @classmethod
    def create(cls, registry_name: str, entry_points: bool = False) -> None:
        """Create a new custom registry."""
        if hasattr(cls, registry_name):
            raise ValueError(f"Registry '{registry_name}' already exists")
        reg = catalogue.create("on_my_gpt", registry_name, entry_points=entry_points)
        setattr(cls, registry_name, reg)
        
__all__ = ["Config", "registry", "ConfigValidationError", "console"]